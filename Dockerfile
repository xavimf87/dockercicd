FROM bitnami/git:2.35.1

RUN curl -sLS "https://dl.k8s.io/release/v1.23.4/bin/linux/amd64/kubectl" -o /usr/bin/kubectl && \
    curl -LO "https://get.helm.sh/helm-v3.8.0-linux-amd64.tar.gz" && \
    tar xvf helm-v3.8.0-linux-amd64.tar.gz && mv linux-amd64/helm /usr/bin/helm

COPY ./bin/yq/v4.4.0/yq_linux_amd64 /usr/bin/yq
COPY ./bin/jq/v1.6/jq-linux64 /usr/bin/jq
COPY ./bin/semver/semver.sh /usr/bin/semver

RUN chmod +x /usr/bin/yq && \
    chmod +x /usr/bin/jq && \
    chmod +x /usr/bin/semver && \
    chmod +x /usr/bin/kubectl && \
    chmod +x /usr/bin/helm && \
    apt-get update && \
    apt-get install -y xmlstarlet exiftool
